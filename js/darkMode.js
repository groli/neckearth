const themeButton = document.getElementById("darkMode");
const themeButtonMobile = document.getElementById("darkModeMobile");
const darkTheme = "dark-mode";
const selectedTheme = localStorage.getItem("selected-theme");

const getCurrentTheme = () => {
  const body = document.getElementsByTagName("body");
  return body[0].classList.contains(darkTheme) ? "dark" : "light";
};

if (selectedTheme === "dark") {
  document.body.classList.add(darkTheme);
} else if (selectedTheme === "light" || !selectedTheme) {
  document.body.classList.remove(darkTheme);
}

themeButton.addEventListener(
  "click",
  (e) => {
    document.body.classList.toggle(darkTheme);
    localStorage.setItem("selected-theme", getCurrentTheme());
    e.stopPropagation();
    window.location.reload();
  },
  false
);

// Responsive view
themeButtonMobile.addEventListener(
  "click",
  (e) => {
    document.body.classList.toggle(darkTheme);
    localStorage.setItem("selected-theme", getCurrentTheme());
    e.stopPropagation();
    window.location.reload();
  },
  false
);
