var App = {};

App.init = function () {
  this.collapse();
};

App.collapse = function () {
  $("body").on("click", ".collapseItem header", function () {
    var li = $(this).parent();
    var opened = li.children(".hideBox").hasClass("opened");

    $(".collapseItem").removeClass("active");

    $(".collapseItem").find(".opened").slideToggle(300).removeClass("opened");

    if (opened == false) {
      li.addClass("active")
        .children(".hideBox")
        .addClass("opened")
        .delay(150)
        .slideToggle(300, function () {
          if ($(window).width() > 767) App.animate(li.offset().top - 0);
          else App.animate(li.offset().top - 0);
        });
    }
  });
};

App.animate = function(posY){
    $('html, body').animate({
        scrollTop: posY
    }, 300);
}

$(document).ready(function () {
  App.init();
});
